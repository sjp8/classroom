
import { Meteor } from 'meteor/meteor'

import { Component, OnInit, OnDestroy } from '@angular/core'
import { Subscription, Observable } from 'rxjs'
import { MeteorObservable } from 'meteor-rxjs'

import { MdDialog, MdDialogRef, MdDialogConfig, MdMenu } from '@angular/material'
import { LoginDialogComponent } from './login-dialog.component'
import { HeaderLinkComponent } from './header-link.component'
import { HeaderLogoComponent } from './header-logo.component'

import { Lectures } from '../../../../both/collections/lectures.collection'
import { Rooms } from '../../../../both/collections/rooms.collection'

@Component({
  selector: 'classroom-header',
  template: `
    <md-toolbar color="primary">
      <header-logo></header-logo>

      <header-link link="lectures" text="Lectures" count="lectureCount" unit="upcoming lectures"></header-link>

      <span class="app-toolbar-filler"></span>

      <login-buttons *ngIf="!user"></login-buttons>

      <account-settings *ngIf="user" [user]="user" [onLogoutClick]="logout"></account-settings>
    </md-toolbar>

    <header-news></header-news>
  `
})

/**
 * The header section of the website including navigation and account functions,
 * and the site logo.
 */
export class HeaderComponent implements OnInit, OnDestroy {
  user: Meteor.User
  autorunSubscription: Subscription

  lectureCountSub: Subscription
  lectureCount: number = 0

  studyRoomCountSub: Subscription
  studyRoomCount: number = 0

  constructor(private dialog: MdDialog) { }

  ngOnInit() {

    this.autorunSubscription = MeteorObservable.autorun().subscribe(() => this.user = Meteor.user())

    this.lectureCountSub = MeteorObservable.subscribe('lectures').subscribe(() => {
      Lectures.find()
        .map(lectures => lectures.length)
        .subscribe(lectureCount => this.lectureCount = lectureCount )
    })

    this.studyRoomCountSub = MeteorObservable.subscribe('rooms').subscribe(() => {
      Rooms.find()
        .map(rooms => rooms.length)
        .subscribe(roomCount => this.studyRoomCount = roomCount)
    })
  }

  ngOnDestroy() {
    this.lectureCountSub.unsubscribe()
    this.studyRoomCountSub.unsubscribe()
  }

}



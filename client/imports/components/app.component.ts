import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router'
import { Title } from '@angular/platform-browser'

@Component({
  selector: 'classroom-app',
  template: `
    <div class="site-content app-content">
      <section class="header-content">
        <classroom-header></classroom-header>
      </section>
      <section class="app-content content">
        <router-outlet></router-outlet>
      </section>
    </div>
  `
})

export class AppComponent implements OnInit {

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private titleService: Title) { }

  ngOnInit() {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .map(() => this.activatedRoute)
      .map(route => {
        while (route.firstChild) route = route.firstChild
        return route
      })
      .filter(route => route.outlet === 'primary')
      .mergeMap(route => route.data)
      .subscribe((data) => {
        // example: NavigationStart, RoutesRecognized, NavigationEnd
        this.titleService.setTitle(data['title'] + ' - Classroom')
      })

  }  
}

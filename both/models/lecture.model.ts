
export interface PaperDrawingEvent {
  x: number
  y: number
  type: string // down (first event), drag (continuation), up (mouse released)
  color?: string // color, if this is the first event
  pen?: string // the type of pen or brush, defaults to "pen". pen | pencil | highlighter | paintbrush | eraser
  time?: number // milliseconds since start
}

export interface LectureSlidesState {
  /** The horizontal slides index. */
  indexh: number
  /** The vertical slides index. */
  indexv: number
  /**
   * The fragment index: -1 if at beginning of slide, null if not a fragented slide,
   * 0+ if any fragments have been shown.
   */
  indexf?: number
  /** True if the overview is being shown. */
  overview: boolean
  /** True if the presentation is paused (blank screen). */
  paused: boolean
}

export interface LectureSlidesData {
  state?: LectureSlidesState
  drawings?: { [key: string]: PaperDrawingEvent[] } // empty object by default
  content: string // the lecture content (html)
  settings?: {
    tool: string // pen, eraser, highlighter
    color: string // hex value or literal
  }
}

export interface Lecture {
  _id?: string
  lecturer: string
  title: string
  link: string
  description: string
  image?: string
  date: Date
  sessionId?: string
  streamId?: string
  mode: string
  slides?: LectureSlidesData
}

export interface LectureQuestion {
  _id?: string
  lecture: string
  answered: boolean
  user: string
  question: string
  date: Date
}

export interface LectureResource {
  _id?: string
  lecture: string
  title: string
  type: string
  content: string
  date: Date
}

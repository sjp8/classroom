
import { Meteor } from 'meteor/meteor'
import { Component, Input, Output, EventEmitter } from '@angular/core'
import { MdDialog, MdDialogRef, MdDialogConfig, MdMenu } from '@angular/material'

@Component({
  selector: 'account-settings',
  template: `
    <span class="username">{{user.username || user.emails[0]?.address}}</span>
    <button md-icon-button [md-menu-trigger-for]="menu">
        <md-icon>account_circle</md-icon>
    </button>

    <md-menu #menu="mdMenu">
      <a [disabled]="true" md-menu-item [routerLink]="['settings']">Settings</a>
      <button [disabled]="true" md-menu-item>Purchases</button>
      <button [disabled]="true" md-menu-item>Support</button>
      <button md-menu-item (click)="logout()">Sign Out</button>
    </md-menu>
  `
})
export class AccountSettingsComponent {
  @Input() user: Meteor.User
  @Input() onLogoutClick = () => {}
  @Output() onLogout = new EventEmitter()

  logout() {
    this.onLogoutClick()
    this.onLogout.emit()
  }
}
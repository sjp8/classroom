
export interface Room {
  title: string
  date: Date
  user: string
  sessionId: string
  content: string
}

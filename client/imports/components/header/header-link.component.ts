
import { Component, Input } from '@angular/core'

@Component({
  selector: 'header-link',
  template: `
    <a md-button [routerLink]="routerLink" [routerLinkActive]="['active-route']">
      {{ text }}
      <span *ngIf="count != null && unit">
        <span
            class='badge'
            [md-tooltip]="countMessage(count, unit)">
          {{lectureCount}}
        </span>
      </span>
    </a>
  `
})

/** A navigation link in the header area. */
export class HeaderLinkComponent {
  @Input() link: string[] | string
  @Input() text: string
  @Input() count: number
  @Input() unit: string

  get routerLink(): string[] {
    return typeof this.link === 'string' ? [this.link] : this.link
  }

  countMessage(count, message) {
    return '' + count + ' ' + message
  }

}

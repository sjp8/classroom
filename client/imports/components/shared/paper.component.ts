
import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, AfterViewInit, ElementRef, ViewChild } from '@angular/core'
import { PaperService } from './paper.service'
import { SocketService } from '../shared/socket.service'
import { PaperDrawingEvent } from '../../../../both/models/lecture.model'

import { Meteor } from 'meteor/meteor'

@Component({
  selector: 'paper-test',
  styles: [
    `
      .paper-container canvas {
        width: 300px;
        height: 300px;
      }
    `
  ],
  template: `
    <div class="paper-container">
      <canvas #canvas></canvas>
    </div>
  `
})
export class PaperComponent implements OnDestroy, AfterViewInit {

  /**
   * The current stroke color.
   */
  @Input() color: string = ['blue', 'red', 'green', 'purple', 'yellow', 'orange'][Math.floor(Math.random() * 6)]

  /**
   * Emits a draw event (x, y, type, color, pen, user, and time) whenever this user draws on the canvas.
   * See createDrawingEvent()
   */
  @Output() draw = new EventEmitter<PaperDrawingEvent>()

  /** The canvas for this paper component for paper.js drawing */
  @ViewChild('canvas') canvas: ElementRef

  /** The start time in milliseconds (by which to measure deltas for history). */
  startTime = Date.now().valueOf()

  /** A map of active paths by user id. */
  userPath: { [userId: string]: any } = {}

  /** Default paper.js Path.simplify() tolerance argument. */
  simplifyTolerance = 1.25

  /** Default paper.js Path.style (also see color input parameter). */
  defaultStrokeStyle = {
    strokeColor: 'blue',
    strokeWidth: 3,
    strokeCap: 'round',
    strokeJoin: 'round'
  }

  constructor(private paper: PaperService) { }

  /** Time in milliseconds since the session began. */
  timestamp() {
    return Date.now().valueOf() - this.startTime
  }

  /** Helper function to create a drawing event that can be handled by handleDrawEvent. */
  createDrawingEvent(paperEvent: any, eventType: string) {
    return {
      x: paperEvent.point.x,
      y: paperEvent.point.y,
      type: eventType,
      color: this.color,
      pen: 'pen',
      user: Meteor.userId(),
      time: this.timestamp()
    }
  }

  /** After the view is initialized, setup paper.js drawing on this component's canvas. */
  ngAfterViewInit() {
    let paper = this.paper.paper
    paper.setup(this.canvas.nativeElement)

    var path: any
    const tool = new paper.Tool()

    // Make the mouse tool register only every 3 pixels, for performance and for better
    // smoothing during drawing.
    tool.minDistance = 3

    // On mouse down, start drawing a new path associated with this user. Emit.
    tool.onMouseDown = (event) => {
      path = new paper.Path()
      path.style = this.defaultStrokeStyle
      path.strokeColor = this.color || this.defaultStrokeStyle.strokeColor
      path.add(event.point)
      this.draw.emit(this.createDrawingEvent(event, 'down'))
    }

    // On mouse drag, continue the path by adding the point and emitting.
    // Smooth to create best visual quality during the draw.
    tool.onMouseDrag = (event) => {
      path.add(event.point)
      path.smooth()
      this.draw.emit(this.createDrawingEvent(event, 'drag'))
    }

    // On mouse release, add the last point, simplify the path, and emit.
    tool.onMouseUp = (event) => {
      path.add(event.point)
      path.simplify(this.simplifyTolerance)
      path = null
      this.draw.emit(this.createDrawingEvent(event, 'up'))
      console.log('draw end')
    }

    // Redraw all paths on every frame.
    paper.view.onFrame = function(event) {
      paper.view.draw()
    }
  }

  ngOnDestroy() {
    // remove paper view and disconnect event listeners (tool)
    var paper = this.paper.paper
    if (paper.tool) {
      paper.tool.remove()
    }
    if (paper.view) {
      paper.view.onFrame = undefined
      paper.view.onResize = undefined
      paper.view.remove()
    }
  }

  /**
   * Handle an external draw event by performing it on the canvas. Used to
   * 
   */
  handleDrawEvent(event) {
    let paper = this.paper.paper
    const type = event.type
    const color = event.color
    if (type == 'down') {
      let path = this.userPath[event.user] = new paper.Path()
      path.style = this.defaultStrokeStyle
      path.strokeColor = event.color || this.defaultStrokeStyle.strokeColor
      path.add(new paper.Point(event.x, event.y))
    } else if (type == 'drag') {
      let path = this.userPath[event.user]
      if (path) {
        path.add(new paper.Point(event.x, event.y))
      }
    } else if (type == 'up') {
      let path = this.userPath[event.user]
      if (path) {
        path.add(new paper.Point(event.x, event.y))
        path.simplify()
        this.userPath[event.user] = undefined
      }
    } else {
      console.error('unsupported draw type', event)
    }
  }
}

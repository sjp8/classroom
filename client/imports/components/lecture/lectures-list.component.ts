
import { Component, OnInit, OnDestroy } from '@angular/core'
import { MdSnackBar } from '@angular/material'

import { Subscription, Observable } from 'rxjs'
import { MeteorObservable } from 'meteor-rxjs'

import { Router } from '@angular/router'

import { ReminderService } from '../shared/reminder.service'
import { Lectures } from '../../../../both/collections/lectures.collection'
import { Lecture } from '../../../../both/models/lecture.model'
import { Reminder } from '../../../../both/models/reminder.model'

const template = `
  <h2>Lectures</h2>
  <md-grid-list cols="2">
    <md-grid-tile *ngFor="let lecture of lectures | async">
      <md-card>
        <md-card-title>{{lecture.title}}</md-card-title>
        <md-card-subtitle>{{lecture.date | amCalendar}}</md-card-subtitle>
        <md-card-actions align="end">
          <button *ngIf="userId && lecture.mode == 'waiting'" md-icon-button color="primary" title="Send Reminder" (click)="toggleReminder(lecture)">
            <md-icon *ngIf="!hasReminder(lecture)">alarm</md-icon>
            <md-icon *ngIf="hasReminder(lecture)">alarm_on</md-icon>
          </button>
          <button *ngIf="userId && lecture.lecturer == userId" md-raised-button color="primary" (click)="navigateToLecture(lecture)">Prepare</button>
          <button class="goto-lecture-action" *ngIf="lecture.mode == 'active'" md-fab color="accent" (click)="navigateToLecture(lecture)">
            <md-icon>play_arrow</md-icon>
          </button>
        </md-card-actions>
      </md-card>
    </md-grid-tile>
  </md-grid-list>
`

@Component({
  selector: 'lectures-list',
  template
})

export class LecturesListComponent implements OnInit, OnDestroy {

  lecturesSub: Subscription
  lectures: Observable<Lecture[]>
  userId: string
  autorunSubscription: Subscription

  constructor(private router: Router, private reminders: ReminderService, private toast: MdSnackBar) { }

  ngOnInit() {
    console.log('has reminder', this.reminders.hasReminder(Meteor.user(), 'lecture', "Zyx7SjTHRgtjHX6td"))
    this.reminders.subscribe()
    this.autorunSubscription = MeteorObservable.autorun().subscribe(() => {
      var newUserId = Meteor.userId()
      if (this.userId != newUserId) {
        this.userId = Meteor.userId()
      }
    })

    this.lecturesSub = MeteorObservable.subscribe('lectures').subscribe(() => {
      this.lectures = Lectures.find({}, { sort: { date: -1 } }).zone()
    })
  }

  /** Whether the user has an existing reminder for the given lecture. */
  hasReminder(lecture: Lecture): boolean {
    return Meteor.userId() && this.reminders.hasReminder(Meteor.user(), ReminderService.LectureReminder, lecture._id)
  }

  /** Toggles the reminder for the given lecture. */
  toggleReminder(lecture: Lecture): Reminder {
    // TODO update reminders dates when lecture date changes. See Issue #7
    if (Meteor.userId()) {
      const user = Meteor.user()
      const reminder = this.reminders.getReminder(user, ReminderService.LectureReminder, lecture._id)
      if (reminder) {
        this.reminders.removeReminder(user, ReminderService.LectureReminder, lecture._id)
        console.log('removed reminder')
        this.toast.open('Cancelled lecture reminder.', 'Undo', { duration: 10000 }).onAction().subscribe(() => {
          const newReminder = this.reminders.addReminder(user, ReminderService.LectureReminder, lecture._id, reminder.date)
          console.log('re-added reminder', newReminder)
        })
      } else {
        const newReminder = this.reminders.addReminder(user, ReminderService.LectureReminder, lecture._id, lecture.date)
        console.log('added reminder', newReminder)

        this.toast.open('Added an email reminder for ' + lecture.title + '.', 'Cancel', { duration: 10000 }).onAction().subscribe(() => {
          this.reminders.removeReminder(user, ReminderService.LectureReminder, lecture._id)
          console.log('cancelled, removing reminder', newReminder)
        })

        return newReminder
      }
    }
  }

  ngOnDestroy() {
    this.lecturesSub.unsubscribe()
    this.autorunSubscription.unsubscribe()
    this.reminders.unsubscribe()
  }

  navigateToLecture(lecture) {
    this.router.navigate(['/lecture', lecture.link])
  }
}

import { Meteor } from 'meteor/meteor'

import { check } from 'meteor/check'
import { opentok, opentokApiKey } from '../../both/imports/opentok'

export function createOpenTokSession() {
  if (!opentok) {
    return null
  }
  const sync = Meteor.wrapAsync(opentok.createSession, opentok)
  return sync({ mediaMode: 'routed', archiveMode: 'always' })
}

export function generateTokenForSession(sessionId: string, role?: string) {
  if (Meteor.isServer) {
    check(sessionId, String)
    if (role != null) {
      check(role, String)
    }
    const token = opentok.generateToken(sessionId, { role: role || 'subscriber' })
    return token
  }
}

export function getApiKey(): string {
  return opentokApiKey
}

Meteor.methods({
  'opentok.createSession': createOpenTokSession,
  'opentok.tokenForSession': generateTokenForSession,
  'opentok.getApiKey': getApiKey
})

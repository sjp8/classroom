
import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'

import { Lecture, LectureQuestion } from '../../../both/models/lecture.model'
import { Lectures, LectureQuestions } from '../../../both/collections/lectures.collection'
import { createOpenTokSession } from '../../../both/methods/opentok.methods'

export function loadLectures() {
  if (Lectures.collection.find({}).count() === 0) {

    const session = createOpenTokSession()
    console.log('session in lecture fixtures', session)

    const lectureUser: Meteor.User = Accounts.findUserByEmail('lecturer@test.com') 
    if (!lectureUser) {
      console.error('lecture user not found for inserting Lectures.')
      return
    }

    const lectureViewerUser: Meteor.User = Accounts.findUserByEmail('user@test.com')

    const lectures: Lecture[] = [
      { mode: 'waiting', lecturer: lectureUser._id, title: "DNA", link: 'dna', description: "DNA Lecture", date: new Date(), sessionId: session.sessionId, slides: { content: exampleLecture } },
      { mode: 'waiting', lecturer: lectureUser._id, title: "Cell Reproduction", link: 'cell-reproduction', description: "Lecture on Cell Reproduction", date: new Date(), sessionId: session.sessionId, slides: { content: exampleLecture2 } }
    ]

    var lectureIds = lectures.map((lecture) => Lectures.collection.insert(lecture))

    if (!lectureIds[0]) {
      console.error('lecture ids not found for inserting lecture questions')
      return
    }

    const lectureQuestions: LectureQuestion[] = [
      { lecture: lectureIds[0], answered: false, user: lectureViewerUser._id, question: 'I was wondering...?', date: new Date() },
      { lecture: lectureIds[0], answered: true, user: lectureViewerUser._id, question: 'I asked a question? And it was answered.', date: new Date() }
    ]

    lectureQuestions.forEach((question: LectureQuestion) => LectureQuestions.collection.insert(question))
  }
}

const exampleLecture = `
  <section>
    <span class="fragment fade-out" data-fragment-index="1">Slide 1</span>
    <span class="fragment fade-out" data-fragment-index="2">Slide 1 (cont).</span>
    <span class="fragment fade-in" data-fragment-index="3">Slide 1 (cont..)</span>
  </section>
  <section>
    <section>Vertical 1</section>
    <section>Vertical 2</section>
  </section>
  <section>
    <h1>Final Slide</h1>
    <ul>
      <li>Lots of content</li>
      <li>Lots</li>
      <li>Of</li>
      <li>Content</li>
      <li>and</li>
      <li>more</li>
    </ul>
    <p>footnote</p>
  </section>
`

const exampleLecture2 = `
  <section>
    <h1>Example 2</h1>
  </section>
  <section>
    <p>Paragraph text with <b>bold</b>.</p>
    <ul>
      <li>a.</li>
      <li>list.</li>
    </ul>
  </section>
`
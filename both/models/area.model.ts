
export interface Area {
  _id?: string
  title: string
  parent: string
}

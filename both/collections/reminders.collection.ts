
import { createCollection } from './createCollection'
import { Reminder } from '../models/reminder.model'

export const Reminders = createCollection<Reminder>("reminders")


import { LectureComponent } from './lecture.component'
import { LecturesListComponent } from './lectures-list.component'
import { LectureQuestionsListComponent } from './lecture-questions-list.component'

export const lectureComponents = [LectureComponent, LecturesListComponent, LectureQuestionsListComponent]

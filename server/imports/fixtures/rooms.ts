
import { Accounts } from 'meteor/accounts-base'

import { Room } from '../../../both/models/room.model'
import { Rooms } from '../../../both/collections/rooms.collection'
import { createOpenTokSession } from '../../../both/methods/opentok.methods'

export function loadRooms() {
  if (Rooms.collection.find({}).count() === 0) {

    const session = createOpenTokSession()
    console.log('session in study rooms fixtures', session)

    const roomCreator: Meteor.User = Accounts.findUserByEmail('user@test.com')
    if (!roomCreator) {
      console.error('normal user not found for inserting Rooms.')
      return
    }

    const rooms: Room[] = [
      { user: roomCreator._id, title: "Question about DNA", date: new Date(), sessionId: session.sessionId, content: "Content So Far.. Should be a JSON object with saved state." },
      { user: roomCreator._id, title: "Cell Reproduction Study Room", date: new Date(), sessionId: session.sessionId, content: "Content for other room." }
    ]

    rooms.forEach((room) => Rooms.collection.insert(room))
  }
}

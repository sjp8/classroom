
import { createCollection } from './createCollection'
import { Room } from '../models/room.model'

export const Rooms = createCollection<Room>("rooms")

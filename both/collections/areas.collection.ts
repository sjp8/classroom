
import { createCollection } from './createCollection'
import { Area } from '../models/area.model'

export const Areas = createCollection<Area>('areas')

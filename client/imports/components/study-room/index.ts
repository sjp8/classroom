
import { StudyRoomComponent }  from './study-room.component'
import { StudyRoomCardComponent } from './study-room-card.component'
import { StudyRoomListComponent } from './study-room-list.component'

export const studyRoomComponents = [StudyRoomComponent, StudyRoomCardComponent, StudyRoomListComponent]
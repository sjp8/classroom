# Live Lectures / Classroom #

A skeleton framework for a Meteor/Angular 2 website delivering synchronized 
lectures with slides, drawing on slides, video, and audio.

### Getting Started ###

* [Install Meteor](https://www.meteor.com/)
* Install dependencies: `npm install`
* Run tests: `npm run test`
* Start the app (localhost:3000): `npm start`
* End to end tests: `npm run protractor`

### Contribution guidelines ###

If you would like to contribute to the project, take a look at the Todos below.
Submit a pull request, I will take the time to integrate any feature that would be useful to others.

### Code Structure ###

The code is structured after the official Meteor guidelines, with client, server, and both folders.

* `client` holds Angular 2 and style code. `imports/components` holds components and their styles, organized
   by area of functionality (e.g., header-related components are in header folder, exposed through HeaderModule).
* `server` holds, most importantly, publications, which define which data is visible to whom.
   Also, MongoDB fixtures (for an initial test database), and server-only functions, namely
   creating an OpenTok session.
* `both` holds isomorphic javascript pertaining to both client and server, including schemas (models),
   methods (functions that are called first on client, then on server)

### Todo ###

* Abstract socket interaction to be library-independent. Currently uses socket.io
* Playback recorded lectures including audio/video, slide transition timing, and real-time drawing.
* Break up lecture component.
* Add more unit tests and calculate code coverage, set up CI.


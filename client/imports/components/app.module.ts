
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common' // also included by default in BrowserModule
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { AppComponent } from './app.component'
import { MaterialModule } from "@angular/material"
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { MomentModule } from 'angular2-moment'
import { studyRoomComponents } from './study-room'
import { OpenTokComponent } from './opentok/opentok.component'
import { homeComponents } from './home'
import { lectureComponents } from './lecture'
import { HeaderModule } from './header/header.module'
import { PaperComponent } from './shared/paper.component'
import { PaperService } from './shared/paper.service'
import { SocketService } from './shared/socket.service'

import { OpenTokService } from './shared/opentok.service'
import { ReminderService } from './shared/reminder.service'
import { SlidesService } from './shared/slides.service'

import { routes } from './app.routes'

@NgModule({
  providers: [
    ReminderService,
    OpenTokService,
    SlidesService,
    SocketService,
    PaperService
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HeaderModule,
    MomentModule,
    FormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [
    AppComponent,
    OpenTokComponent, // used by multiple modules, included here
    PaperComponent, // paperjs component (testing)
    ...homeComponents, // Issue #6 * deprecate in favor of specific pages
    ...studyRoomComponents, // * study rooms module
    ...lectureComponents // * lectures module
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }

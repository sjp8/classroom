
import { Area } from '../../../both/models/area.model'
import { Areas } from '../../../both/collections/areas.collection'

export function loadAreas() {
  if (Areas.collection.find({}).count() === 0) {
    const areas: Area[] = [
      { title: 'Biological and Biochemical Foundations', parent: null },
      { title: 'Chemical and Physical Foundations', parent: null },
      { title: 'Psychological, Social and Biological Foundations', parent: null },
      { title: 'Critical Analysis and Reasoning', parent: null }
    ]

    areas.forEach((area) => Areas.insert(area))
  }
}


import { Injectable } from '@angular/core'

declare var paper;

@Injectable()
export class PaperService {

  get paper() { return paper }
}

import { Component } from '@angular/core'

@Component({
  selector: 'header-logo',
  template: `
    <img class='apple-logo' src="/images/apple.png" alt="Classroom Apple" />
    <h4>Classroom</h4>
  `
})
export class HeaderLogoComponent { }
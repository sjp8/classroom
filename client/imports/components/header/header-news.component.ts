
import { Component } from '@angular/core'

@Component({
  selector: 'header-news',
  template: `
    <section class="news">
      <!-- TODO dynamically generated suggestions for new content. -->
      <a md-button class="news-item" [routerLink]="['lecture', 'dna']">
        Lecture: DNA
      </a>
    </section>
  `
})
/** A section just below the main header that shows suggested content. */
export class HeaderNewsComponent { }
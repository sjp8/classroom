
import { Meteor } from 'meteor/meteor'

const OpenTok = Meteor.isServer ? require('opentok') : null
const OpenTokWrapper = Meteor.isServer ? require('../../server/imports/opentok') : null

export const opentokApiKey = Meteor.isServer ? OpenTokWrapper.opentokApiKey : "45774432" // TODO synchronize server/client config See Issue #5.
export const opentokApiSecret = Meteor.isServer ? OpenTokWrapper.opentokApiSecret : null

export const opentok = Meteor.isServer ? new OpenTok(opentokApiKey, opentokApiSecret) : null

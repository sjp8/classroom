
console.log('overall e2e test')

import * as chai from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
chai.use(chaiAsPromised)
import { protractor, browser, by, element } from 'protractor'

const expect = chai.expect
const should = chai.should()

describe('a protractor test', () => {

  beforeEach(function(done) {
    this.timeout(6000)
    browser.get('http://localhost:3000', 6000).thenFinally(done)
    browser.waitForAngular('waiting for angular 2')
  })

  it('should change the page title on navigation', function() {
    expect(browser.getTitle()).to.eventually.equal('Landing - Classroom')
    element(by.css('a[href="/lectures"]')).click()
    return browser.getTitle().should.eventually.equal('Lectures - Classroom')
  })
})

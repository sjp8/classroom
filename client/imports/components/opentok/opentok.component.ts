
import {
  Component, ElementRef, OnInit, OnDestroy, OnChanges, 
  SimpleChanges, Input, Output, ViewChild, AfterViewInit, 
  EventEmitter
} from '@angular/core'

import { OpenTokService } from '../shared/opentok.service'
import { Meteor } from 'meteor/meteor'

@Component({
  selector: 'opentok-viewer',
  template: `
    <div class="opentok-viewer">
      <div class="access-dialog-open" *ngIf="accessDialogOpen">Please grant camera and microphone access.</div>
      <div #openTokElement class="opentok-viewer-container"></div>
    </div>
  `
})

export class OpenTokComponent implements OnDestroy, OnChanges {
  /** The session id to use for opentok. */
  @Input() sessionId: string

  /** Whether opentok should publish this user's camera and microphone */
  @Input() publish: boolean = false

  /** Whether to stream the lecture's main stream (from Lecture mongo doc) */
  @Input() stream: boolean = true

  /** The stream id to subscribe to when stream is true */
  @Input() streamId: string

  /** Notifies observers that the session is connected, sending the Session object. */
  @Output() sessionConnected = new EventEmitter<any>()

  /** Notifies observers that the publisher has started publishing a new stream. */
  @Output() streamCreated = new EventEmitter<any>()

  @ViewChild('openTokElement') openTokElement: ElementRef

  accessDialogOpen: boolean = false

  /** The OpenTok session. */
  session: any

  /** The OpenTok publisher (creates a stream and makes it accessible to anyone connected to the session). */
  publisher: any // TODO opentok client side types. See Issue #3

  /** Data about the current stream being published or consumed. */
  streamObj: any
  /** The stream subscription (listening to changes or new streams). */
  streamSub: any

  constructor(private opentok: OpenTokService) { }

  /**
   * Starts the session. Should be done as soon as sessionId is available.
   */
  startSession() {
    // get a new opentok session id from the server, init locally
    this.session = this.opentok.getSessionFromId(this.sessionId)

    // listen for new streams
    this.session.on('streamCreated', (event) => {
      if (this.stream) {
        // unsubscribe any previous streams
        if (this.streamObj && this.streamSub) {
          this.session.unsubscribe(this.streamSub)
        }

        // update subscription to latest from lecturer
        this.streamObj = event.stream
        this.streamSub = this.opentok.subscribeToStream(this.openTokElement.nativeElement, this.session, this.streamObj)
      }
    })

    // Connect to the session on OpenTok with the correct permissions
    this.opentok.getTokenForSession(this.sessionId, this.publish ? 'publisher' : 'subscriber', (err, token) => {
      this.session.connect(token, (err) => {
        // TODO handle all opentok errors on client side. See Issue #2
        if (err) {
          return console.error("Session connect error", err)
        }

        // Session established.

        // notify component listeners
        this.sessionConnected.emit(this.session)

        // prepare for the start of the lecture by creating a publisher or starting to listen for streams
        if (this.publish) {
          // Initialize before session connected. See https://tokbox.com/developer/guides/publish-stream/js/
          this.publisher = this.opentok.getPublisher(this.openTokElement.nativeElement, () => {
            this.startPublishing()
          })
        }
      })
    })
  }

  /** Starts publishing. */
  startPublishing() {
    // Publish stream associated with session.
    this.session.publish(this.publisher, (err) => {
      if (err) {
        return console.error('Session publish error', err)
      }

      // Notify component listeners
      this.streamCreated.emit(this.publisher.stream)
    })
  }

  /** Called when changes are made to component's @Input() parameters. */
  ngOnChanges(changes: SimpleChanges) {
    // OnChanges is called before OnInit
    if (changes['publish'] && changes['publish'].currentValue) {
      // if publish is true, start the session
      this.startSession()
    } else if (changes['stream'] && changes['stream'].currentValue) {
      // if stream is true, start the session
      this.startSession()
    } else if (this.session && this.session.connection && !this.publish && !this.stream) {
      // if there is a session, and the component is neither set to be publishing nor streaming,
      // end the session
      this.endSession()
    }
  }

  /** Ends the OpenTok session. */
  endSession() {
    try {
      if (this.publisher) {
        this.publisher.destroy().then(() => {
          this.session.disconnect()
        })
      } else {
        if (this.session && this.session.connection) {
          this.session.disconnect()
        }
      }
    } catch (ex) {
      console.error('Error destroying opentok session.', ex)
    }
  }

  /** End the session when the component is destroyed. */
  ngOnDestroy() {
    this.endSession()
  }
}


import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { Lecture, LectureQuestion } from '../../../../both/models/lecture.model'
import { LectureQuestions } from '../../../../both/collections/lectures.collection'
import { MdSnackBar } from '@angular/material'

import { Observable, Subscription } from 'rxjs'
import { MeteorObservable } from 'meteor-rxjs'
import { Meteor } from 'meteor/meteor'

@Component({
  selector: 'lecture-questions-list',
  template: `
    <md-list *ngIf="!private && lecture && userId == lecture.lecturer">
      <h3 md-subheader>Audience Questions</h3>
      <md-list-item *ngFor="let question of lectureQuestions | async" class="lecture-question">
        <img md-list-avatar [src]="userAvatarById()" alt="Image of {{usernameById(question.user)}}" />
        <h3 md-line>{{usernameById(question.user)}}</h3>
        <p md-line>{{question.question}}</p>
        <button md-icon-button color="primary" (click)="markAnswered(question)" md-tooltip="Mark As Answered" mdTooltipPosition="left">
          <md-icon class="material-icons">done</md-icon>
        </button>
      </md-list-item>

      <md-divider></md-divider>
      <h3 md-subheader>Answered Questions</h3>

      <md-list-item *ngFor="let question of answeredLectureQuestions | async" class="answered-lecture-question">
        <h3 md-line>{{usernameById(question.user)}}</h3>
        <p md-line>{{question.question}}</p>
      </md-list-item>
    </md-list>
    <md-list *ngIf="private && lecture.lecturer != userId">
      <md-list-item *ngFor="let question of lectureQuestions | async" class="lecture-question">
        <img md-list-avatar [src]="userAvatarById()" alt="Image of {{usernameById(question.user)}}" />
        <h3 md-line>{{usernameById(question.user)}}</h3>
        <p md-line>{{question.question}}</p>
        <button md-icon-button color="primary" (click)="markAnswered(question)" md-tooltip="My question was answered" mdTooltipPosition="left">
          <md-icon class="material-icons">done</md-icon>
        </button>
      </md-list-item>
    </md-list>
  `
})

export class LectureQuestionsListComponent implements OnInit, OnDestroy {

  @Input() lecture: Lecture
  @Input() showNotifications: boolean = false
  @Input() private: boolean = false

  userId: string

  lectureQuestions: Observable<LectureQuestion[]>
  answeredLectureQuestions: Observable<LectureQuestion[]>
  questionsUsers: Observable<Meteor.User[]>

  questionsSub: Subscription

  constructor(private toast: MdSnackBar) { }

  usernameById(userId: string): string {
    var user = Meteor.users.findOne(userId)
    if (user && (user._id == Meteor.userId())) {
      return 'Me'
    } else {
      return user && ((user.emails && user.emails[0]) ? user.emails[0].address : user.username) || 'Unknown'
    }
  }

  userAvatarById(userId: string): string {
    var user = Meteor.users.findOne(userId)
    // TODO default anonymous image, or generate avatars randomly on registration.
    return (user && user.profile && user.profile.avatar) ? user.profile.avatar : '/images/anonymous.png'
  }

  markAnswered(question: LectureQuestion) {
    this.updateAnswered(question, true)
    if (this.showNotifications) {
      this.toast.open('Question marked answered.', 'Undo', { duration: 10000 }).onAction().subscribe(() => {
        console.log('lecture-questions-list.component - Undo mark question called.')
        this.updateAnswered(question, false)
      })
    }
  }

  updateAnswered(question: LectureQuestion, answered: boolean): void {
    LectureQuestions.update({ _id: question._id }, { $set: { answered: answered } })
  }

  ngOnInit() {
    this.userId = Meteor.userId()
    var lastToast = new Date()
    this.questionsSub = MeteorObservable.subscribe('lectureQuestionsAll').subscribe(() => {
      if (this.private) {
        this.lectureQuestions = LectureQuestions.find({ lecture: this.lecture._id, user: this.userId, answered: false }, { sort: { date: -1 } }).zone()
      } else {
        this.lectureQuestions = LectureQuestions.find({ lecture: this.lecture._id, answered: false }, { sort: { date: -1 } }).zone()
      }
      this.lectureQuestions.subscribe(() => {
        if (this.showNotifications) {
          var criteria: any = { date: { $gt: lastToast }, lecture: this.lecture._id, answered: false }
          if (this.private) {
            criteria.user = this.userId
          }
          var newer = LectureQuestions.findOne(criteria)
          if (newer) {
            this.toast.open('New question asked.', null, { duration: 5000 })
            lastToast = new Date()
          }
        }
      })
      this.answeredLectureQuestions = LectureQuestions.find({ lecture: this.lecture._id, answered: true }).zone()
      console.log('example question from lecture id ' + this.lecture._id, LectureQuestions.findOne({ lecture: this.lecture._id }))
    })
  }

  ngOnDestroy() {
    this.questionsSub.unsubscribe()
  }

}


import { Component, Input, OnInit, OnDestroy, OnChanges, SimpleChanges, AfterViewInit, ElementRef, ViewChild } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Meteor } from 'meteor/meteor'
import { Subscription }  from "rxjs"
import { MeteorObservable } from 'meteor-rxjs'

import { SlidesService } from '../shared/slides.service'
import { SocketService } from '../shared/socket.service'

import { Lectures, LectureQuestions } from '../../../../both/collections/lectures.collection'
import { Lecture, LectureQuestion } from '../../../../both/models/lecture.model'

@Component({
  selector: 'lecture',
  template: `
    <md-card class="lecture-container for-lecturer" *ngIf="lecture && lecturer">
      <md-card-title>Your Lecture Entitled: {{lecture.title}} ({{lecture.mode}})</md-card-title>
      <md-card-subtitle>{{pastTime() ? 'Started' : 'Starts'}} {{lecture.date | amTimeAgo}}. {{lecture.description}}</md-card-subtitle>
      <md-card-content>
        <button *ngIf="lecture.mode == 'waiting' && overdue()" md-button color="accent" (click)="startLecture()">Start Broadcasting</button>
        <md-checkbox [checked]="startAutomatically" *ngIf="lecture.mode == 'waiting' && !overdue()" md-button color="accent" (change)="startAutomatically = $event.checked">Start Broadcasting Automatically</md-checkbox>
        <button *ngIf="lecture.mode == 'active'" md-button (click)="endLecture()">End Lecture</button>
      </md-card-content>
    </md-card>

    <md-card class="lecture-container for-viewer" *ngIf="lecture && !lecturer">
      <md-card-title>{{lecture.title}}</md-card-title>
      <md-card-subtitle>{{pastTime() ? 'Started' : 'Starts'}} {{lecture.date | amTimeAgo}}. {{lecture.description}}</md-card-subtitle>
      <button md-icon-button (click)="slides.enterFullScreen()"><md-icon>fullscreen</md-icon></button>
    </md-card>

    <md-card class="slides-container">
      <div #slidesElement class="reveal">
        <div class="slides" #slidesContentElement></div>
        <opentok-viewer *ngIf="lecture && lecturer" [publish]="lecture.mode == 'active'" [stream]="false" [sessionId]="lecture.sessionId" (streamCreated)="onStreamCreated($event)" (sessionConnected)="onSessionConnected($event)"></opentok-viewer>
        <opentok-viewer *ngIf="lecture && !lecturer" [sessionId]="lecture.sessionId" (sessionConnected)="onSessionConnected($event)" [publish]="false" [stream]="true" [streamId]="lecture.streamId"></opentok-viewer>
      </div>
      <div class="reveal-actions" *ngIf="lecture && lecturer">
        <button md-icon-button mdTooltip="Clear Annotations" (click)="clearCurrentDrawing()"><md-icon>flip</md-icon></button>
        <button md-icon-button mdTooltip="Undo" (click)="slides.undo(); emit('undo');" [disabled]="!slides.canUndo()"><md-icon>undo</md-icon></button>
        <button md-icon-button mdTooltip="Redo" (click)="slides.redo(); emit('redo');" [disabled]="!slides.canRedo()"><md-icon>redo</md-icon></button>
        <button class="color-action" *ngFor="let color of colors" md-icon-button (click)="setColor(color)" [class.selected]="color == currentColor" [style.color]="color"><md-icon>brightness_1</md-icon></button>
        <button class="tool-action" md-icon-button mdTooltip="Pen" [class.selected]="currentTool == 'pen'" (click)="setTool('pen')"><md-icon>mode_edit</md-icon></button>
        <button class="tool-action" md-icon-button mdTooltip="Highlighter" [class.selected]="currentTool == 'highlighter'" (click)="setTool('highlighter')"><md-icon>highlighter</md-icon></button>
      </div>
    </md-card>

    <md-card *ngIf="lecture && lecturer">
      <lecture-questions-list [lecture]="lecture" [showNotifications]="true"></lecture-questions-list>
    </md-card>

    <md-card *ngIf="lecture && !lecturer">
      <md-input-container>
        <textarea md-input md-autosize #questionInput placeholder="Question" [maxLength]="300"></textarea>
      </md-input-container>
      <button md-button (click)="askQuestion(questionInput.value); questionInput.value = '';"><md-icon>question_answer</md-icon> Ask Question</button>
      <lecture-questions-list [lecture]="lecture" [private]="true" [showNotifications]="false"></lecture-questions-list>
    </md-card>
  `
})

export class LectureComponent implements AfterViewInit, OnInit, OnDestroy {

  @ViewChild('slidesElement') slidesElement: ElementRef
  @ViewChild('slidesContentElement') slidesContentElement: ElementRef

  startAutomatically: boolean = false
  lecture: Lecture
  lectureContent: string
  userId: string
  lecturer: boolean = false
  paramsSub: Subscription
  lectureSub: Subscription
  autorunSub: Subscription
  startTimeout: any
  colors: string[] = ['black', 'blue', 'red', 'green', 'orange', 'purple', 'yellow']
  currentColor: string
  currentTool: string
  socketCallback: Function

  constructor(private route: ActivatedRoute, private slides: SlidesService, private socket: SocketService) {
    console.log(this.slides)
  }

  /** Update lecture component's user info. */
  updateUser() {
    if (this.userId != Meteor.userId()) {
      this.userId = Meteor.userId()
      this.lecturer = this.lecture ? (this.userId && this.userId == this.lecture.lecturer) : null
      console.log('updating user info', [this.userId, this.lecturer])
      this.updateSlidesConfig()
    }
  }

  updateSlidesConfig() {
    if (this.lecturer) {
      this.slides.enableKeyboardShortcuts()
      this.slides.enableDrawing()
    } else {
      this.slides.disableKeyboardShortcuts()
      this.slides.disableDrawing()
    }
  }

  /** Update slide state given current lecture database object slide state. */
  updateSlideState() {
    if (this.slides.isReady() && this.lecture && this.lecture.slides) {
      var state = this.lecture.slides.state
      if (state) {
        console.log('updateSlideState')
        this.slides.setState(this.lecture.slides.state)
      }
    }
  }

  /** Setup timers that watch for the begining of the lecture. */
  setupTimers() {
    if (this.startTimeout) {
      clearTimeout(this.startTimeout)
    }

    if (this.lecture.mode == 'waiting') {
      // if the lecture is still waiting to start,
      // set a timer to start publishing/viewing automatically (based on user type)
      var difference = this.lecture.date.getTime() - new Date().getTime()
      this.startTimeout = setTimeout(() => {
        if (this.lecturer) {
          if (this.startAutomatically) {
            this.startLecture()
          } 
        } else {
          this.updateSlideState()
        }
      }, difference)
    } else if (this.lecture.date.getTime() < new Date().getTime()) {
      console.error('Lecture is not waiting, even though start date is in the future. This should not happen.')
      this.updateLectureMode('waiting') // it's in the future, should be "waiting".TODO remove in production.
    }
  }

  ngOnInit() {
    // observe changes to lecture id parameter
    this.paramsSub = this.route.params
      .map(params => params['lectureId'])
      .subscribe(lectureId => {
        if (this.lectureSub) {
          this.lectureSub.unsubscribe()
        }

        // subscribe to this lecture's data
        this.lectureSub = MeteorObservable.subscribe('lecture', lectureId).subscribe(() => {
          console.log('subscribed to lecture id', lectureId)

          if (this.autorunSub) {
            this.autorunSub.unsubscribe()
          }

          // listen for any changes to lecture object such as slides state change
          // and listen to changes to user object in case lecturer has logged in.
          this.autorunSub = MeteorObservable.autorun().subscribe(() => {
            console.log('Lecture.component - autorun')
            this.lecture = Lectures.findOne({ $or: [{ _id: lectureId }, { link: lectureId }] })

            // update user and derived data, slide state
            this.updateUser()
            if (!this.lecturer && this.lecture.mode == 'active') {
              // only set slide state for viewers. Lecturers own the slide state.
              this.updateSlideState()
            }

            // If there has been a change of lecture content (to fix a bug or problem found during a lecture)
            // reload the slides content
            this.updateSlides(true)
          })

          // TODO temporarily, all lectures are 'waiting' and start 15 seconds from now.
          //      in order to facilitate testing.
          this.updateLectureMode('waiting')
          this.lecture.date = new Date(new Date().valueOf() + 15 * 1000)
          Lectures.update({_id: this.lecture._id}, { $set: { date: this.lecture.date } })

          // setup timers for actions waiting for the lecture's start date/time.
          this.setupTimers()
        })
      })
  }


  ngOnDestroy() {
    this.paramsSub.unsubscribe()
    this.autorunSub.unsubscribe()
    if (this.lectureSub) {
      this.lectureSub.unsubscribe()
    }

    if (this.socketCallback) {
      this.socket.socket.removeListener('lecture-event', this.socketCallback)
    }

    this.slides.remove()
  }

  /** OpenTok session connected successfully. Called from opentok component output. */
  onSessionConnected(session: any) {
    console.log('LectureComponent - connected to opentok session: ', session)
  }

  /** 
   * Stream created (new published stream). Called from opentok component output.
   * This enables new viewers to connect automatically to the correct stream.
   */
  onStreamCreated(stream: any) {
    console.log('LectureComponent - stream was created by publisher', stream)
    this.lecture.streamId = stream.streamId
    Lectures.update({ _id: this.lecture._id }, { $set: { streamId: this.lecture.streamId } })
  }

  /** Lecture mode and timing management. */
  updateLectureMode(mode) {
    this.lecture.mode = mode // TODO test whether or not this manual update is necessary.
    // is the original object updated when update is called?
    Lectures.update({ _id: this.lecture._id }, { $set: { mode: mode } })
  }

  /**
   * Change lecture mode to 'active', which starts broadcasting lecture on opentok, and
   * TODO synchronizes users' slides from the title slide, to the lecturer's current slide.
   */
  startLecture() {
    this.updateLectureMode('active')
  }

  /**
   * Ends the lecture. Changes state, ends opentok streaming, and
   * TODO returns to title slide.
   */
  endLecture() {
    if (this.lecture.mode == 'active') {
      this.updateLectureMode('ended')
    }
  }

  /** True if lecture is waiting to start and should have already started based on its date/time. */
  overdue() {
    return this.lecture && this.lecture.mode == 'waiting' && this.lecture.date.getTime() < new Date().getTime()
  }

  /** True if the lecture's start date/time has passed. */
  pastTime() {
    return this.lecture && this.lecture.date.getTime() < new Date().getTime()
  }

  /** Asking a question. */
  askQuestion(question) {
    LectureQuestions.insert({
      lecture: this.lecture._id,
      answered: false,
      user: this.userId,
      question: question,
      date: new Date()
    })
  }
  
  /**
   * Drawing management.
   */
  clearCurrentDrawing() {
    this.slides.clearCurrentDrawing()
    this.saveDrawingData()
    this.emitEvent({ list: 'clear' })
  }

  setColor(color: string) {
    this.currentColor = color
    this.slides.setColor(color)
  }

  setTool(tool: string) {
    this.currentTool = tool
    this.slides.setTool(tool)
  }

  saveDrawingData() {
    const drawingData = this.slides.getDrawingData()
    let slidesData = this.lecture.slides || { state: null, drawings: {} }
    slidesData.drawings = drawingData

    Lectures.update({_id: this.lecture._id }, { $set: {
      slides: slidesData
    }})
  }

  /**
   * Update slides if the lecture content has changed.
   */
  updateSlides(performSetup: boolean = false) {
    const content = (this.lecture && this.lecture.slides) ? this.lecture.slides.content : null
    console.log('updateSlides. perform setup: ' + performSetup + ', content: ' + (content ? content.substring(0, 15) : 'none'))
    if (content && (!this.lectureContent || this.lectureContent != content)) {
      console.log('setting content')
      // setup slides
      this.lectureContent = this.lecture.slides.content
      this.slidesContentElement.nativeElement.innerHTML = content
      if (performSetup) {
        this.slides.setup(this.slidesElement, () => { return this.lecturer })
      }
    }
  }

  /**
   * Initialize view-dependent listeners: slide state, slides ready, and synchronizing drawing events.
   * TODO can drawing events be synchronized within slides service or a new component e.g. SynchronizedSlidesComponent
   */
  ngAfterViewInit() {

    setTimeout(() => {
      // set reveal flags that are based on lecturer/viewer status.
      this.slides.onReady(() => {
        console.log('slides ready. initializing settings and listeners.')

        this.slides.setDrawingData(this.lecture.slides.drawings || {})

        this.updateSlidesConfig() // enable or disable shortcuts and drawing

        if (this.lecturer || this.lecture.mode == 'active') {
          this.updateSlideState() // "turn the page" to the correct slide.
        }

        // Set initial tool and color based on lecture or default values
        var slideSettings = this.lecture.slides && this.lecture.slides.settings || { tool: this.slides.getTool(), color: this.slides.getColor() }
        this.setColor(slideSettings.color)
        this.setTool(slideSettings.tool)

        // if this is the lecturer, emit draw point events to be synchronized
        // by any listening viewers.
        this.slides.onDrawPoint((type: string, point: any, style: any, delta: any) => {
          if (this.lecturer) {
            var lecture = this.lecture._id
            // TODO make sure that other users cannot emulate emitting drawing.
            this.emitEvent({ list: 'draw', type, value: { point, style } })
          }
        })

        // watch for new paths to be drawn by lecturer, updating the object each time with all drawing data
        this.slides.onDrawPath((pathData) => {
          if (this.lecturer) {
            this.saveDrawingData()
          }
        })

        // watch for lecturer to change color and drawing tool, broadcasting changes via socket.io
        this.slides.onDrawSetting((event) => {
          if (this.lecturer) {
            this.emitEvent(event)
            Lectures.update({ _id: this.lecture._id }, { $set: {
              'slides.settings.tool': this.slides.getTool(),
              'slides.settings.color': this.slides.getColor()
            }})
          }
        })

        // if this is the viewer, events should be handled (listening to socket draw events, instructing
        // slides service to handle each draw event).
        this.socketCallback = (event) => {
          if (!this.lecturer && event.lecture == this.lecture._id) {
            if (event.list == 'draw') {
              console.log('handle draw event', event)
              this.slides.handleDrawPoint(event)
            } else if (event.list == 'setting') {
              console.log('handle draw setting', event)
              this.slides.handleDrawSetting(event)
            } else if (event.list == 'clear') {
              console.log('handle clear canvas')
              this.slides.clearCurrentDrawing()
            } else if (event.list == 'undo') {
              console.log('handle undo')
              this.slides.undo()
            } else if (event.list == 'redo') {
              console.log('handle redo')
              this.slides.redo()
            }
          }
        }

        this.socket.socket.on('lecture-event', this.socketCallback)

        // watch for changes to slide state, and broadcast changes if this is the lecturer's doing.
        // these changes are listened to by the tracker/autorun subscription in ngOnInit.
        this.slides.onStateChange((state) => {
          console.log('ngAfterViewInit - lecture.component - state change fired', JSON.stringify(state))
          if (this.lecturer) {
            var slidesData = this.lecture.slides || { state: null, drawings: {} }
            slidesData.state = state

            Lectures.update({ _id: this.lecture._id }, { $set: {
              slides: slidesData
            }})
          }
        })
      })
    }, 1000)
  }

  /** Emits an event which is relayed by socket.io to all viewers. */
  emitEvent(event) {
    event.lecture = this.lecture._id
    this.socket.socket.emit('lecture-event', event)
    this.logEvent(event.list, event.type, event.value)
  }

  /** Emits an event which is relayed by socket.io to all viewers. */
  emit(list, type, value) {
    var event = {
      lecture: this.lecture._id,
      list, type, value
    }

    this.emitEvent(event)
  }

  /** Logs a lecture event with a timestamp. Used to replay archived lectures. */
  logEvent(list, type, value) {
    console.log('logEvent', arguments)
    this.socket.socket.emit('lecture-log', {
      lecture: this.lecture._id, list, type, value
    })
  }

  playbackLog() {
    // retrieve list of logs in chronological order
    // set timeouts for all events, or use requestAnimationFrame and use time to unshift from beginning of list.
    // play archived video
  }
}

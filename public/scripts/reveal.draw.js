
var RevealDrawCreate = function(revealObject) {
  var draw = revealDrawFactory()
  draw.initialize(revealObject)
  return draw
}

function revealDrawFactory() {
  /** The reveal.js object */
  var reveal

  /** Canvas DOM elements indexed by indexh-indexv */
  var canvases

  /** PaperScope objects indexed by indexh-indexv */
  var scopes

  /** JSON drawings to load as default data for each slide. (Optional.) */
  var drawings

  /** The size of the canvas, according to reveal config. */
  var size

  /** If enabled, mouse events on canvas will draw lines. */
  var drawingEnabled = true

  /** Callbacks for drawing events. (see onDrawPath and onDrawPoint setter functions) */
  var callbacks = {
    onDrawPath: function() { }, // on mouse up, after a new shape has been fully drawn.
    onDrawPoint: function() { } // on mouse up/down/drag, whenever a new point is added to canvas.
  }

  /** The current drawing color. */
  var strokeColor = 'blue'

  var undoStack = {}
  var redoStack = {}

  /** The current tool */
  var currentTool = 'pen' // default tool is pen

  /** Available tools for drawing/modifying the canvas. */
  var tools = ['pen', 'highlighter', 'eraser']

  /**
   * Initialize reveal draw with the given Reveal object, and optional drawing data
   * 
   * @param{any[]} drawingData A nested array of Paper.js exported JSON strings corresponding to 
   *               the horizontal and vertical indices of the slides.
   */
  function initialize(revealObject, drawingData) {
    reveal = revealObject
    drawings = drawingData || {}

    scopes = {}
    canvases = {}

    size = { 
      width: reveal.getConfig().width,
      height: reveal.getConfig().height
    }

    reveal.addEventListener('ready', function(event) {
      updateSlide()
    })
    reveal.addEventListener('slidechanged', function(event) {
      updateSlide()
    })
  }

  /**
   * Update the slide to show the appropriate paper.js drawing.
   * Called when reveal changes slides.
   */
  function updateSlide() {
    console.log('update slide paperjs')
    var state = reveal.getState()
    setForSlide(state.indexh, state.indexv)
  }

  /**
   * Get the PaperScope object associated with the given
   * horizontal and vertical Reveal slide indices.
   * Attaches a canvas DOM element to slide.
   */
  function getScope(indexh, indexv, slide) {
    var canvas = getCanvas(indexh, indexv, slide)
    var scope = scopes['' + indexh + '-' + indexv]
    if (!scope) {
      scope = new paper.PaperScope()
      scopes['' + indexh + '-' + indexv] = scope
      loadDrawingIfAvailable(scope, indexh, indexv)
      scope.setup(canvas)
      setupPaperDrawing(scope)
    }

    return scope
  }

  /**
   * If drawing data was provided in the constructor, and is available for the given slide,
   * load it into the PaperScope object.
   */
  function loadDrawingIfAvailable(scope, indexh, indexv) {
    if (drawings) {
      var horizontal = drawings[horizontal]
      if (horizontal) {
        var drawing
        if (Array.isArray(horizontal)) {
          drawing = horizontal[indexv]
        } else if (indexv === 0) {
          drawing = horizontal
        }

        if (drawing) {
          scope.project.importJSON(drawing)
        }
      }
    }
  }

  /** The current path being drawn, or null if the mouse is not active. */
  var path

  /**
   * Sets up drawing capabilities on a PaperScope and
   * its view (the canvas). Must already have a view setup.
   */
  function setupPaperDrawing(paper) {
    paper.view.viewSize.width = size.width
    paper.view.viewSize.height = size.height

    function pointObject(p) {
      return { x: p.x, y: p.y }
    }

    function absolutePoint(mouseEvent) {
      return new paper.Point({
        x: mouseEvent.offsetX,
        y: mouseEvent.offsetY
      })
    }

    var tool = new paper.Tool()
    tool.onMouseDown = function(event) {
      if (drawingEnabled) {
        var result = handleMouseEvent('down', event.point, paper, event.delta)
        if (result) {
          const style = {}
          const fields = ['strokeWidth', 'strokeColor', 'strokeCap', 'strokeJoin']
          fields.forEach((styleField) => {
            style[styleField] = path.style[styleField]
          })
          callbacks.onDrawPoint('down', pointObject(absolutePoint(event.event)), style)
        }
      }
    }
    tool.onMouseDrag = function(event) {
      var result = handleMouseEvent('drag', event.point, paper, event.delta)
      if (result) {
        callbacks.onDrawPoint('drag', pointObject(absolutePoint(event.event)))
      }
    }
    tool.onMouseUp = function(event) {
      console.log('tool mouse up', event.event)
      var pathRef = path
      var result = handleMouseEvent('up', event.point, paper, event.delta)
      path = null
      if (result) {
        console.log('should be calling draw path callback')
        callbacks.onDrawPoint('up', pointObject(absolutePoint(event.event)))
        callbacks.onDrawPath(pathRef.pathData)
      }
    }

    paper.view.onFrame = function(event) {
      paper.view.draw()
    }
  }

  function scaleAbsolutePoint(absolutePoint) {
    var scale = reveal.getScale()
    return {
      x: absolutePoint.x * scale,
      y: absolutePoint.y * scale
    }
  }

  function handleMouseEvent(type, point, paper, delta) {
    console.log('mouse event', { x: point.x, y: point.y, scale: reveal.getScale() })
    point = new paper.Point(point) // make sure all formats convert to paper.Point
    var scale = reveal.getScale()
    function scalePoint(p) {
      return new paper.Point(p.x / scale, p.y / scale)
    }
    var scaledPoint = scalePoint(point)
    var tool = getTool()

    function eraseIn(path) {
      var items = paper.project.getItems({
          'class': paper.Path
      })

      for (var i = 0; i < items.length; i++) {
          var item = items[i]
          if (item != path) {
              var newItem = item.subtract(path)
              if (newItem) {
                  item.replaceWith(newItem)
              }
          }
      }
    }

    if (type == 'down') {
      if (tool == 'eraser') {
        paper.tool.fixedDistance = 10
        path = new paper.Path()
        path.fillColor = {
          hue: 0,
          saturation: 0,
          brightness: 0.5,
          alpha: 0.5
        }

      	path.add(scaledPoint)
      } else {
        paper.tool.fixedDistance = undefined
        redoStack = []
        path = new paper.Path()
        path.style = {
          strokeColor: strokeColor,
          strokeWidth: tool == 'pen' ? 3 : 32,
          strokeCap: 'round',
          strokeJoin: 'round'
        }
        if (tool == 'highlighter') {
          path.style.strokeColor.alpha = 0.3
        }
        path.add(scaledPoint)
        return true
      }
    } else if (type == 'drag') {
      if (path) {
        if (tool == 'eraser') {
          console.log('points', [point, delta])
          var step = scalePoint(delta).divide(2)
          step.angle += 90

          var middlePoint = scaledPoint.add(scalePoint(delta).divide(2))
          var top = middlePoint.add(step)
          var bottom = middlePoint.subtract(step)

          path.add(top)
          path.insert(0, bottom)
          path.smooth()
        } else {
          path.add(scaledPoint)
          path.smooth()
          return true
        }
      }
    } else if (type == 'up') {
      if (path) {
        if (tool == 'eraser') {
        	path.add(scaledPoint)
          path.closed = true
          path.smooth()
          eraseIn(path)
          path.remove()

        } else {
          path.add(scaledPoint)
          path.simplify(1)
        }

        initializeUndoStack()
        undoStack[slideKey()].push(path)
        path = null
        return true
      }
    }

    return false
  }

  /**
   * Gets the slide's DOM element at the given horizontal
   * and vertical reveal indices.
   */
  function getSlide(indexh, indexv) {
    return reveal.getSlide(indexh, indexv)
  }

  /**
   * Gets the canvas object at the given horizontal
   * and vertical reveal indices. If one does not exist,
   * creates a new canvas and appends it to slide.
   */
  function getCanvas(indexh, indexv, slide) {
    var key = '' + indexh + '-' + indexv
    var canvas = canvases[key]
    if (canvas) {
      return canvas
    } else {
      canvas = document.createElement('canvas')
      canvas.id = "revealdraw-" + key
      slide.appendChild(canvas)
      canvas.style.width = size.width
      canvas.style.height = size.height
      canvases[key] = canvas
      return canvas
    }
  }

  /** Get the key for the given indices. */
  function _key(indexh, indexv) { return '' + indexh + '-' + indexv }

  /** Get the indices [indexh, indexv] for the given key. */
  function _parts(key) {
    var parts = key.split('-')
    return [Number(parts[0]), Number(parts[1])]
  }

  /**
   * Sets up the slide at given horizontal and
   * vertical reveal indices with paper drawing.
   * Called by updateSlide.
   */
  function setForSlide(indexh, indexv) {
    var slide = getSlide(indexh, indexv)
    var scope = getScope(indexh, indexv, slide)
  }

  function getDrawingData() {
    var data = {}
    for (var key in scopes) {
      var json = scopes[key].project.exportJSON()
      data[key] = json
    }

    return data
  }

  /** Clears the drawing on the given slide. */
  function clearDrawing(indexh, indexv) {
    var slide = getSlide(indexh, indexv)
    var scope = getScope(indexh, indexv, slide)
    if (scope) {
      scope.project.clear()
    }
  }

  /** Set the color of the pen. */
  function setColor(color) {
    strokeColor = color
  }

  /** Get the current color of the pen. */
  function getColor() {
    return strokeColor
  }

  function initializeUndoStack() {
    var key = slideKey()
    undoStack[key] = undoStack[key] || []
    redoStack[key] = redoStack[key] || []
  }

  function slideKey() {
    var state = reveal.getState()
    var key = '' + state.indexh + '-' + state.indexv
    return key
  }

  /** Undo the last operation. */
  function undo() {
    initializeUndoStack()
    var key = slideKey()
    var path = undoStack[key].pop()
    if (path) {
      redoStack[key].push(path)
      path.visible = false
    }
  }
  
  /** Redo an undone draw operation. */
  function redo() {
    initializeUndoStack()
    var key = slideKey()
    var path = redoStack[key].pop()
    if (path) {
      undoStack[key].push(path)
      path.visible = true
    }
  }

  /** Is there an undone operation that can be redone. */
  function canRedo() {
    var stack = redoStack[slideKey()]
    return stack && stack.length > 0
  }

  /** Is there a draw operation that can be undone. */
  function canUndo() {
    var stack = undoStack[slideKey()]
    return stack && stack.length > 0
  }

  /** Sets the current tool (pen, highlighter, eraser). */
  function setTool(tool) {
    if (tools.indexOf(tool) !== -1) {
      currentTool = tool
    }
  }

  /** Get the current drawing tool. */
  function getTool() {
    return currentTool
  }

  /** Sets the drawing data for a particular slide. */
  function setDrawingDataForSlide(indexh, indexv, data) {
    console.log('setDrawingDataForSlide', [indexh, indexv, data])
    var key = _key(indexh, indexv)
    var slide = getSlide(indexh, indexv)
    var scope = getScope(indexh, indexv, slide)
    if (scope) {
      var project = scope.project
      project.clear()
      try {
        console.log('importing JSON', project.view)
        project.activate()
        project.importJSON(data)
      } catch (ex) {
        console.error('Error importing paper.js JSON', data)
      }
    }
  }

  /** Sets drawing data for all slides, indexed by key. */
  function setDrawingData(data) {
    for (var key in data || {}) {
      var indices = _parts(key)
      setDrawingDataForSlide(indices[0], indices[1], data[key])
    }
  }

  return {
    initialize: initialize.bind(this),
    onDrawPath: function(callback) {
      callbacks.onDrawPath = callback
    },
    onDrawPoint: function(callback) {
      callbacks.onDrawPoint = callback
    },
    enableDrawing: function() { drawingEnabled = true },
    disableDrawing: function() { drawingEnabled = false },
    clearDrawing: clearDrawing,
    getColor: getColor,
    setColor: setColor,
    setTool: setTool,
    getTool: getTool,
    undo: undo,
    redo: redo,
    canRedo: canRedo,
    canUndo: canUndo,
    getDrawingData: getDrawingData,
    setDrawingData: setDrawingData,
    scaleAbsolutePoint: scaleAbsolutePoint,
    performMouseEvent: function(type, point) {
      var state = reveal.getState()
      var scope = getScope(state.indexh, state.indexv, getSlide(state.indexh, state.indexv))
      handleMouseEvent(type, point, scope)
    }
  }
}
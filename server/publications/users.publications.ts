
import { Meteor } from 'meteor/meteor'

Meteor.publish('user', function() {
  if (!this.userId) {
    return this.ready()
  } else {
    return Meteor.users.find({ _id: this.userId })
  }
})

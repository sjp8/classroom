
import { Meteor } from 'meteor/meteor'
import { loadAreas } from './imports/fixtures/areas'
import { loadLectures } from './imports/fixtures/lectures'
import { loadUsers } from './imports/fixtures/users'
import { loadRooms } from './imports/fixtures/rooms'

import { Log } from '../both/models/log.model'
import { Logs } from '../both/collections/logs.collection'

import * as http from 'http'
import * as socket_io from 'socket.io'


Meteor.startup(() => {
    console.log('meteor startup loading fixtures')
    loadUsers()
    loadLectures()
    loadAreas()
    loadRooms()

    const server = http.createServer()
    const io = socket_io(server)

    io.on('connection', Meteor.bindEnvironment(function(socket) {
        console.log('socket.io connect - new socket client')
        socket.on('disconnect', () => console.log('socket.io disconnect'))
        socket.on('lecture-event', (data) => {
            console.log('lecture event', JSON.stringify(data))
            socket.broadcast.emit('lecture-event', data)
        })
        socket.on('lecture-log', Meteor.bindEnvironment(function(event) {
            console.log("lecture log", JSON.stringify(event))
            Logs.insert({
                table: 'lectures',
                list: event.list,
                type: event.type,
                value: event.value
            })
        }))
    }))

    try {
        server.listen(8080)
    } catch (ex) {
        console.error(ex)
    }
})

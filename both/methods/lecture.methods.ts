
import { Meteor } from 'meteor/meteor'
import { Lecture, LectureResource, LectureQuestion } from '../models/lecture.model'
import { Lectures, LectureResources, LectureQuestions } from '../collections/lectures.collection'
import { SimpleSchema } from 'meteor/aldeed:simple-schema'
import { check } from 'meteor/check'
import { createOpenTokSession } from '../methods/opentok.methods'
import * as OpenTok from 'opentok'

// create a lecture
  // create a permanent session for this lecture

export function createLecture(title: string, description: string, userId: string, date: Date, mode: string = 'waiting'): Lecture {
  new SimpleSchema({
    title: { type: String, min: 5, max: 200 },
    description: { type: String, min: 5, max: 400 },
    date: { type: Date }
  }).validate({ title, description, date })

  // TODO check user level
  // if (!Lecture.canCreate(this.userId)) {
  //   throw new Meteor.Error('lecture.methods.createLecture.unauthorized',
  //     'Must be an authorized user to create a lecture.')
  // }

  const session = Meteor.isServer ? createOpenTokSession() : null
  const link = getLectureLinkForTitle(title)

  const lectureId = Lectures.collection.insert({
    title: title as string,
    lecturer: userId,
    link,
    description: description as string,
    date,
    sessionId: session ? session.sessionId as string : null,
    mode: 'waiting'
  })
  const lecture: Lecture = Lectures.findOne(lectureId)
  return lecture
}

export function getLecture(lectureId: string): Lecture {
  check(lectureId, String)
  const byId = Lectures.findOne(lectureId)
  if (byId) {
    return byId
  } else {
    return Lectures.findOne({ link: lectureId })
  }
}

export function getLectureLinkForTitle(title: string): string {
  var attempt = 0
  while (attempt <= 3) {
    const stub = title.replace(/[^a-zA-Z0-9 ]/g, '').replace(/\s/g, '-').toLowerCase() + (attempt > 0 ? '-' + attempt : '')
    if (stub.length === 0) {
      return null // if there were no valid characters, there is clearly an error
    }

    if (Lectures.collection.find({ link: stub }).count() === 0) {
      return stub
    }

    attempt++
  }

  return "" + title + Math.floor(Math.random() * 1000)
}

export function createLectureResource(lecture: string, title: string, type: string, content: string): LectureResource {
  new SimpleSchema({
    lecture: { type: String },
    title: { type: String, min: 3, max: 100 },
    type: { type: String, allowedValues: ['image', 'slides'] },
    content: { type: String },
  }).validate({ title, type, content })

  const resourceId = LectureResources.collection.insert({
    lecture, title, type, content, date: new Date()
  })

  const resource: LectureResource = LectureResources.findOne(resourceId)
  return resource
}

export function getResourcesForLecture(lecture: string): LectureResource[] {
  return LectureResources.collection.find({ lecture }).fetch()
}

export function setLectureSlidesState(lectureId: string, state: any) {
  const lecture: Lecture = Lectures.findOne(lectureId)
  Lectures.update({ _id: lecture }, { $set: {
    'slides.state': state
  }})
}

Meteor.methods({
  'lecture.create': createLecture,
  'lecture.get': getLecture,
  'lecture.resource.create': createLectureResource,
  'lecture.resources.get': getResourcesForLecture,
  'lecture.slides.setState': setLectureSlidesState
})

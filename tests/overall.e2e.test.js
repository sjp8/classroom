"use strict";
console.log('overall e2e test');
var chai = require("chai");
var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
var protractor_1 = require("protractor");
var expect = chai.expect;
var should = chai.should();
describe('a protractor test', function () {
    beforeEach(function (done) {
        this.timeout(6000);
        protractor_1.browser.get('http://localhost:3000', 6000).thenFinally(done);
        protractor_1.browser.waitForAngular('waiting for angular 2');
    });
    it('should change the page title on navigation', function () {
        expect(protractor_1.browser.getTitle()).to.eventually.equal('Landing - Classroom');
        protractor_1.element(protractor_1.by.css('a[href="/lectures"]')).click();
        return protractor_1.browser.getTitle().should.eventually.equal('Lectures - Classroom');
    });
});

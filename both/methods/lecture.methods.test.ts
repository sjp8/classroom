
import { createLecture, createLectureResource, getLectureLinkForTitle, getLecture, getResourcesForLecture } from './lecture.methods'
import { Lecture, LectureResource, LectureQuestion } from '../models/lecture.model'
import { Lectures, LectureQuestions, LectureResources } from '../collections/lectures.collection'

import { chai } from 'meteor/practicalmeteor:chai'
import { Meteor } from 'meteor/meteor'
import { resetDatabase } from 'meteor/xolvio:cleaner'

Meteor.methods({
  'test.resetDatabase': () => resetDatabase(),
})

const expect = chai.expect

describe('lectures', () => {
    beforeEach((done) => {
        Meteor.call('test.resetDatabase', done)
    })

    it('should create a new lecture and return it', () => {
        if (Meteor.isServer) {
            const lecture: Lecture = createLecture('Example Lecture', 'Example Description.', "notreallyauserid", new Date())
            expect(lecture).to.exist
            expect(lecture._id).to.be.a('string')
            expect(lecture.title).to.be.a('string')
        } else {
            createLecture('Example Client Lecture', 'Example Client Description', "notreallyauserid", new Date())
        }
    })

    it('should properly create search engine-friendly link stubs', () => {
        const titles = [
            ['1st Lecture on DNA', '1st-lecture-on-dna'],
            ['#$*&#$*&#', null],
            ['1st Lecture on DNA', '1st-lecture-on-dna-1'],
            ['Mix of Speci@l Characters', 'mix-of-specil-characters']
        ]

        titles.forEach(function(pair) {
            const link = getLectureLinkForTitle(pair[0])
            expect(link).to.equal(pair[1])
            Lectures.collection.insert({ title: pair[0], description: pair[0], lecturer: "notreallyauserid", date: new Date(), link: link })
        })
    })

    it("should retrieve a lecture by its ID or link stub", () => {
        const lecture: Lecture = createLecture("Example Lecture Title", "......", "notreallyauserid", new Date())
        expect(lecture).to.exist
        const findById = getLecture(lecture._id)
        const findByLink = getLecture(lecture.link)

        expect(findById._id).to.equal(lecture._id)
        expect(findByLink._id).to.equal(lecture._id)
    })
})

describe('lecture resources', () => {
    it('should create a resource with the given lecture and content', () => {
        const lecture: Lecture = createLecture('a lecture with resources', 'lecture with resources', null, new Date())
        expect(lecture).to.exist
        const resource: LectureResource = createLectureResource(lecture._id, 'create resource', 'image', 'content')
        expect(resource).to.exist
        expect(resource.date).to.exist
        expect(resource.title).to.equal('create resource')
        expect(resource.lecture).to.equal(lecture._id)
    })
    
    it('should retrieve all resources for the given lecture', () => {
        const lecture: Lecture = createLecture('lecture with resources', 'lecture with resources', null, new Date())
        const lectureResources: LectureResource[] = [
            createLectureResource(lecture._id, 'first image resource', 'image', 'an image.png'),
            createLectureResource(lecture._id, 'second slides resource', 'slides', '{"slides": []}')
        ]

        const retrievedResources: LectureResource[] = getResourcesForLecture(lecture._id)
        expect(retrievedResources).to.have.lengthOf(2)
        expect(retrievedResources[0].title).to.equal('first image resource')
    })
})

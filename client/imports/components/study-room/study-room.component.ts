
import { Component, Input } from '@angular/core'
import { Room } from '../../../../both/models/room.model'

@Component({
  selector: 'study-room',
  template: `
    <div>{{room.title}} started on {{room.date}}</div>
  `
})

export class StudyRoomComponent {
  @Input() room: Room
}

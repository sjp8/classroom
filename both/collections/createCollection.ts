
import { MongoObservable } from 'meteor-rxjs'

export function createCollection<T>(name: string): MongoObservable.Collection<T> {
  return new MongoObservable.Collection<T>(name)
  // Meteor.isServer? new Mongo.Collection<T>(name) : 
}


import { Meteor } from 'meteor/meteor'
import { opentokApiKey } from '../../../../both/imports/opentok'
import { Injectable } from '@angular/core'

declare var OT: any

// See https://tokbox.com/developer/sdks/js/reference/Session.html
@Injectable()
export class OpenTokService {
    opentok: any
    apiKey: string = opentokApiKey // TODO config from server. See Issue #1 and Issue #5
    defaultSubscribeOptions: any = {
        width: '100%',
        height: '100%',
        publishAudio: true,
        publishVideo: true,
        insertMode: 'append',
        usePreviousDeviceSelection: true // IE only 
    }

    constructor() {
        this.opentok = OT
    }

    getSessionFromId(sessionId) {
        return this.opentok.initSession(this.apiKey, sessionId)
    }

    getTokenForSession(sessionId, role, callback) {
        Meteor.call('opentok.tokenForSession', sessionId, role || null, callback)
    }

    getPublisher(targetElement, callback) {
        return this.opentok.initPublisher(targetElement, this.defaultSubscribeOptions, callback)
    }

    subscribeToStream(targetElement, session, stream) {
        return session.subscribe(stream, targetElement, this.defaultSubscribeOptions)
    }

    getDevices(callback) {
        return this.opentok.getDevices(callback)
    }
}

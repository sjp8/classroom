
import { createCollection } from './createCollection'
import { Log } from '../models/log.model'

export const Logs = createCollection<Log>("logs")

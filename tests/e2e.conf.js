
exports.config = {
  framework: 'mocha',
  specs: ['overall.e2e.test.js'],
  seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
  capabilities: {
    browserName: "chrome"
  },
  useAllAngular2AppRoots: true,
  rootElement: 'classroom-app'
}

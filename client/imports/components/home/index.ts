
import { HomeComponent } from './home.component'
import { StudyRoomListComponent } from '../study-room/study-room-list.component'
import { LecturesListComponent } from '../lecture/lectures-list.component'

export const homeComponents = [HomeComponent, LecturesListComponent, StudyRoomListComponent]


import { Meteor } from 'meteor/meteor'
import { MeteorObservable } from 'meteor-rxjs'
import { Injectable } from '@angular/core'
import { Reminders } from '../../../../both/collections/reminders.collection'
import { Reminder } from '../../../../both/models/reminder.model'
import { Observable, Subscription } from 'rxjs'

@Injectable()
export class ReminderService  {
  reminderSub: Subscription
  autorunSub: Subscription
  userId: string

  static LectureReminder: string = "lecture"

  subscribe() {
    if (!this.reminderSub || this.reminderSub.closed) {
      this.unsubscribe()
    }
    this.reminderSub = MeteorObservable.subscribe('userReminders').subscribe(() => {
      this.userId = Meteor.userId()
      console.log('reminders: ', Reminders.collection.find({user: this.userId}).fetch())
    })
  }

  unsubscribe() {
    if (this.reminderSub) {
      this.reminderSub.unsubscribe()
    }
  }
  
  getReminder(user: Meteor.User, type: string, identifier: string): Reminder {
    return Reminders.findOne({ user: user && user._id, type, identifier })
  }

  hasReminder(user: Meteor.User, type: string, identifier: string): boolean {
    if (!user) {
      return false
    }
    return this.getReminder(user, type, identifier) != null
  }

  /** Adds a reminder, which will remind the user in a timely fashion by email of a given event
   * @param {Meteor.User} user The user to add a reminder for.
   * @param {string} type The type of reminder (e.g. lecture, sale).
   * @param {string} identifier The mongo ID for the reminder, if applicable.
   * @param {Date} date The date of the actual event.
   * @returns {Reminder} The created Reminder object.
   */
  addReminder(user: Meteor.User, type: string, identifier: string, date: Date): Reminder {
    if (!user) {
      return null
    }
    const existingReminder = Reminders.collection.findOne({ type, identifier, user: user._id })
    if (existingReminder) {
      console.log('returning existing reminder')
      Reminders.collection.update({ _id: existingReminder._id }, { $set: { date: date } })
      return Reminders.collection.findOne(existingReminder._id)
    } else {
      const reminderId: string = Reminders.collection.insert({ user: user._id, type, identifier, created: new Date(), date })
      return Reminders.findOne(reminderId)
    }
  }

  /** Remove the given reminder. */
  removeReminder(user: Meteor.User, type: string, identifier: string) {
    if (user) {
      const reminder = this.getReminder(user, type, identifier)
      if (reminder) {
        return Reminders.collection.remove({ _id: reminder._id })
      }
    }
  }

  /** Return an observable for the user's reminders. */
  reminders(user: Meteor.User): Observable<Reminder[]> {
    if (!user) {
      return null
    }
    return Reminders.find({ user: user._id }).zone()
  }
}

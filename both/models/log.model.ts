
export interface Log {
  _id?: string
  table: string
  list: string
  type: string
  value?: any
}

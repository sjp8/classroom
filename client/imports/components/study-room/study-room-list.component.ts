
import { Component, OnInit, OnDestroy, HostBinding, 
         trigger, transition, animate, 
         style, state } from '@angular/core'

import { Rooms } from '../../../../both/collections/rooms.collection'
import { Room } from '../../../../both/models/room.model'

import { Subscription, Observable } from 'rxjs'
import { MeteorObservable } from 'meteor-rxjs'

const template = `
  <h3>Study Rooms</h3>
  <md-card *ngFor="let room of rooms | async">
    <md-card-title>{{room?.title}}</md-card-title>
    <md-card-subtitle>{{room?.date}}</md-card-subtitle>
  </md-card>
`

@Component({
  selector: 'study-room-list',
  template,
  animations: [
    trigger('routeAnimation', [
      state('*', style({ opacity: 1, transform: 'translateX(0)' }) ),
      transition(':enter', [
        style({ opacity: 0, transform: 'translateX(-100%)' }),
        animate('0.2s 0.5s ease-in')
      ]),
      transition(':leave', [
        animate('0.5s ease-out', style({ opacity: 0, transform: 'translateY(100%)' }))
      ])
    ])
  ]
})
export class StudyRoomListComponent implements OnInit, OnDestroy {
  rooms: Observable<Room[]>

  roomsSub: Subscription

  @HostBinding('@routeAnimation') get routeAnimation() { return true }
  @HostBinding('style.display') get display() { return 'block' }
  @HostBinding('style.position') get position() { return 'absolute' }

  ngOnInit() {
    this.roomsSub = MeteorObservable.subscribe('rooms').subscribe(() => {
      this.rooms = Rooms.find({}, { sort: { date: -1 } }).zone()
    })
  }

  ngOnDestroy() {
    this.roomsSub.unsubscribe()
  }
}

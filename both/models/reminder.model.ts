
export interface Reminder {
  _id?: string
  user: string
  type: string
  identifier: string
  created: Date
  date: Date
}

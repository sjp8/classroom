
import { Injectable, ElementRef } from '@angular/core'

declare var RevealCreate: any
declare var RevealDrawCreate: any

@Injectable()
export class SlidesService {
  reveal: any
  revealDraw: any
  stateChangedListener: any
  stateChangeEvents: string[] = ['slidechanged', 'fragmentshown', 'fragmenthidden', 'overviewshown', 'overviewhidden']
  readyListeners: { (): void; } [] = []
  drawSettingCallback: { (event: { list: string, type: string, value: string | number }): void; } = function() { }

  setup(elementRef: ElementRef, keyboardCondition: () => boolean) {
    if (this.reveal) {
      this.remove()
    }
    const width = 960, height = 700

    this.reveal = RevealCreate()
    this.reveal.initialize({
      aspectRatio: height / width,
      preventScrolling: false,
      mouseWheel: false,
      keyboardCondition: keyboardCondition,
      controls: true,
      overview: false,
      center: false,
      embedded: true,
      help: false,
      history: false,
      height: height,
      width: width,
      transition: 'slide',
      backgroundTransition: 'slide',
      margin: 0,
      minScale: 0.2,
      maxScale: 1.5
    })
    this.revealDraw = RevealDrawCreate(this.reveal)
    console.log('reveal initialized')
  }

  isReady() {
    return this.reveal && this.reveal.isReady()
  }

  onReady(func: () => void) {
    if (this.isReady()) {
      func()
    } else {
      this.reveal.addEventListener('ready', func)
      this.readyListeners.push(func)
    }
  }

  enterFullScreen() {
    if (this.isReady()) {
      this.reveal.enterFullscreen()
    }
  }

  enableDrawing() {
    if (this.isReady()) {
      this.revealDraw.enableDrawing()
    }
  }
  disableDrawing() {
    if (this.isReady()) {
      this.revealDraw.disableDrawing()
    }
  }

  remove() {
    if (this.stateChangedListener) {
      this.stateChangeEvents.forEach((state) => this.reveal.removeEventListener(state, this.stateChangedListener))
      this.stateChangedListener = null
    }
    this.readyListeners.forEach((listener) => {
      this.reveal.removeEventListener('ready', listener)
    })
    this.reveal = null
  }

  /**
   * Disable all keyboard shortcuts, making presentation simply embedded (controlled programmatically).
   */
  disableKeyboardShortcuts() {
    if (this.isReady()) {
      console.log('slides.service - disable keyboard shortcuts')
      this.reveal.configure({ keyboard: false, controls: false, touch: false })
    }
  }

  enableKeyboardShortcuts() {
    if (this.isReady()) {
      console.log('slides.service - enable keyboard shortcuts')
      this.reveal.configure({ keyboard: true, controls: true, touch: true })
    }
  }

  /**
   * Get the current slides state, e.g. the current location in the
   * presentation and status of various modes.
   */
  getState() {
    var state = this.isReady() ? this.reveal.getState() : null
    return state
  }

  /**
   * Function to call when the state (slide or fragment) changes.
   * Removed when remove is called.
   */
  onStateChange(callback) {
    this.stateChangedListener = (event) => callback(this.reveal.getState())
    this.stateChangeEvents.forEach(state => this.reveal.addEventListener(state, this.stateChangedListener))
  }

  /** Function to call when a point is drawn on a slide. */
  onDrawPoint(callback) {
    if (this.isReady()) {
      this.revealDraw.onDrawPoint(callback)
    }
  }

  /** Function to call when a draw setting is changed. */
  onDrawSetting(callback) {
    this.drawSettingCallback = callback
  }

  /** Draw an onDrawPoint event programmatically on the current slide. */
  handleDrawPoint(event) {
    if (this.isReady()) {
      this.revealDraw.performMouseEvent(event.type, this.revealDraw.scaleAbsolutePoint(event.value.point), event.value.style)
    }
  }

  /** Handles a setting change (e.g. change tool or color). */
  handleDrawSetting(event) {
    if (this.isReady()) {
      if (event.type == 'tool') {
        this.setTool(event.value)
      } else if (event.type == 'color') {
        this.setColor(event.value)
      } else {
        console.log('unknown setting event', event)
      }
    }
  }

  /** Function to call when a path is completed on a slide. */
  onDrawPath(callback) {
    if (this.isReady()) {
      this.revealDraw.onDrawPath(callback)
    }
  }

  /**
   * Get drawing data from the reveal draw object.
   */
  getDrawingData() {
    return this.isReady() && this.revealDraw.getDrawingData()
  }

  /**
   * Set drawing data for the reveal draw object.
   * Re-renders or renders drawings as needed (if different source,
   * clear project for given scope/canvas, )
   */
  setDrawingData(drawingData) {
    if (this.isReady()) {
      this.revealDraw.setDrawingData(drawingData)
    }
  }

  /** Sets the drawing color. */
  setColor(color: string) {
    if (this.isReady()) {
      this.revealDraw.setColor(color)
      this.drawSettingCallback({ list: 'setting', type: 'color', value: color })
    }
  }

  /** Returns the current drawing color. */
  getColor(): string {
    return this.revealDraw.getColor()
  }

  setTool(tool: string) {
    if (this.isReady()) {
      this.revealDraw.setTool(tool)
      this.drawSettingCallback({ list: 'setting', type: 'tool', value: tool })
    }
  }

  getTool(): string {
    return this.revealDraw && this.revealDraw.getTool()
  }

  /** Undo the last draw operation. */
  undo() { this.revealDraw && this.revealDraw.undo() }

  /** Redo an undone draw operation. */
  redo() { this.revealDraw && this.revealDraw.redo() }

  /** Is there a draw operation that can be undone? */
  canUndo() { return this.revealDraw && this.revealDraw.canUndo() }

  /** Is there an undone operation that can be redone? */
  canRedo() { return this.revealDraw && this.revealDraw.canRedo() }

  /** Removes  */
  clearCurrentDrawing() {
    if (this.isReady()) {
      var state = this.getState()
      this.revealDraw.clearDrawing(state.indexh, state.indexv)
    }
  }

  /**
   * Set the slides state or location, updating the UI. Should be in
   * the same format as state returned by getState.
   */
  setState(state: any) {
    if (this.isReady()) {
      this.reveal.setState(state)
    }
  }
}

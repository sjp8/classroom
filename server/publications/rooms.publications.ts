
import { Meteor } from 'meteor/meteor'
import { Rooms } from '../../both/collections/rooms.collection'
import { Room } from '../../both/models/room.model'

Meteor.publish('room', (roomId: string) => {
  return Rooms.find({ _id: roomId })
})

Meteor.publish('rooms', () => {
  return Rooms.find({})
})

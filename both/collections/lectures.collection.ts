
import { createCollection } from './createCollection'
import { Lecture, LectureQuestion, LectureResource } from '../models/lecture.model'

export const Lectures = createCollection<Lecture>('lectures')
export const LectureQuestions = createCollection<LectureQuestion>('lecturequestions')
export const LectureResources = createCollection<LectureResource>('lectureresources')

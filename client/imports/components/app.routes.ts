import { Route } from '@angular/router'
import { HomeComponent } from './home/home.component'
import { LectureComponent } from './lecture/lecture.component'
import { LecturesListComponent } from './lecture/lectures-list.component'

export const routes = [
  { path: '', component: LecturesListComponent, data: { title: 'Lectures' } },
  { path: 'lectures', component: LecturesListComponent, data: { title: 'Lectures' } },
  { path: 'lecture/:lectureId', component: LectureComponent, data: { title: 'Viewing Lecture' } }
]

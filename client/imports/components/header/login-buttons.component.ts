
import { Meteor } from 'meteor/meteor'

import { Component, Output, EventEmitter } from '@angular/core'
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material'

import { LoginDialogComponent } from './login-dialog.component'

@Component({
  selector: 'login-buttons',
  template: `
    <button md-raised-button class="md-accent" (click)="showLogin()">Sign In</button>
    <button md-button (click)="showRegister()">Register</button>
  `
})

/** Displays header links to help a user get signed in either by logging in or registering. */
export class LoginButtonsComponent {
  /** Called on successful login or register with the user object. */
  @Output() onLogin = new EventEmitter()

  dialogRef: MdDialogRef<LoginDialogComponent>
  config: MdDialogConfig = {
    disableClose: false
  }

  constructor(private dialog: MdDialog) { }

  showLogin() {
    this.dialogRef = this.dialog.open(LoginDialogComponent, this.config)
    this.dialogRef.afterClosed().subscribe(success => {
      console.log(success ? 'logged in successfully and dialog closed' : 'not logged in, dialog cancelled')
      this.onLogin.emit(Meteor.user())
    })
  }


}
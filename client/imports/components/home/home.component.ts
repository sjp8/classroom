
import { Component } from '@angular/core'

@Component({
  selector: 'home',
  template: `
    <div>
      <h2>Home</h2>
      <h3>Lectures</h3>
      <lectures-list></lectures-list>
    </div>
  `
})

export class HomeComponent {

}

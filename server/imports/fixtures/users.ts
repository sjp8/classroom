
import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'

export function loadUsers() {
  if (Meteor.users.find({}).count() === 0) {
    const users = ['viewer@test.com', 'lecturer@test.com', 'admin@test.com', 'user@test.com', 'moderator@test.com']
    const password = 'testing'

    users.forEach(user => Accounts.createUser({ email: user, password: 'testing' }))
  }
}


import { Injectable } from '@angular/core'
import * as io from 'socket.io-client'

@Injectable()
export class SocketService {
  socket: any
  constructor() {
    const socket = this.socket = io('http://localhost:8080')
    socket.on('connect', () => console.log('socket.io connected'))
    socket.on('disconnect', () => console.log('socket.io disconnected'))
  }

}

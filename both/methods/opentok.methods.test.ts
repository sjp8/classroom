import { chai } from 'meteor/practicalmeteor:chai'
import { Meteor } from 'meteor/meteor'
import { opentok } from '../../server/imports/opentok'

const should = chai.should()
const expect = chai.expect

import { createOpenTokSession, generateTokenForSession, getApiKey } from './opentok.methods'

describe('opentok methods', function() {
    it('creates a session from scratch and returns sessionId', () => {
        const session: any = createOpenTokSession()
        if (Meteor.isServer) {
            expect(session).to.exist
            expect(session.sessionId).to.be.a('string')
        } else {
            expect(session).to.not.exist
        }
    })

    it('generates a token for a session', function() {
        const session = createOpenTokSession()
        if (Meteor.isServer) {
            const token = opentok.generateToken(session.sessionId)
            expect(token).to.exist
        } else {
            const token = opentok.generateToken(session ? session.sessionId : null)
            expect(token).to.not.exist
        }
    })

    it('returns the opentok api key', function() {
        const apiKey: string = getApiKey()
        expect(apiKey).to.be.a('string')
    })
})
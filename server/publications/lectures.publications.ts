
import { Meteor } from 'meteor/meteor'
import { Mongo } from 'meteor/mongo'
import { Accounts } from 'meteor/accounts-base'

import { Lectures, LectureQuestions } from '../../both/collections/lectures.collection'
import { Lecture, LectureQuestion } from '../../both/models/lecture.model'

Meteor.publish('lecture', (lectureId: string) => {
  return Lectures.find({ $or: [{ _id: lectureId }, { link: lectureId }] })
})

Meteor.publish('lectures', () => {
  return Lectures.find({})
})

Meteor.publish('lectureQuestionsAll', () => {
  return LectureQuestions.find()
})

Meteor.publishComposite('lectureQuestions', (lectureId) => {
  return {
    find() {
      return LectureQuestions.collection.find({ lecture: lectureId })
    },
    children: [
      {
        find(question: LectureQuestion) {
          return Meteor.users.find({ _id: question.user })
        }
      }
    ]
  }
})

LectureQuestions.allow({
  update(userId, doc, fieldNames, modifier) {
    var lecture = Lectures.collection.findOne(doc.lecture)
    var fieldAllowed = fieldNames.every(fieldName => ['answered', 'question'].indexOf(fieldName) !== -1)

    // the user can update their own doc, and so can the lecturer
    return fieldAllowed && (doc.user == userId || lecture.lecturer == userId)
  },
  insert(userId, doc) {
    // TODO validate against the model
    return doc.user == userId
  }
})

Lectures.allow({
  update(userId, doc, fieldNames, modifier) {
    var user = Accounts.findUserByEmail('admin@test.com')
    return userId == user._id || userId == doc.lecturer
  },
  insert(userId, doc) {
    var user = Accounts.findUserByEmail('admin@test.com')
    return userId == user._id
  }
})


import { Component, Input } from '@angular/core'

//import { Rooms } from '../../../../both/collections/rooms.collection'
import { Room } from '../../../../both/models/room.model'

@Component({
  selector: '[study-room-card]',
  template: `
    <div class="study-room-card">
      <button md-fab><md-icon>check circle</md-icon></button>
      <md-card-title>{{room?.title}}</md-card-title>
      <md-card-subtitle>{{room?.date}}</md-card-subtitle>
    </div>
  `
})
export class StudyRoomCardComponent {
  @Input() room: Room
}


import { Meteor } from 'meteor/meteor'
import { Accounts } from 'meteor/accounts-base'

import { Component, Optional, Input, OnInit, OnDestroy } from '@angular/core'
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material'

@Component({
  selector: 'login-dialog',
  template: `
    <div>
      <h2 md-dialog-title>Login</h2>
      <md-input-container>
        <input md-input placeholder="Username" [(ngModel)]="username" />
        <md-hint color="warn" [hidden]="!error.username">{{error.username}}</md-hint>
      </md-input-container>
      <md-input-container>
        <input md-input type="password" placeholder="Password" [(ngModel)]="password" (keyup.enter)="attemptLogin(username, password)">
        <md-hint color="warn" [hidden]="!error.password">{{error.password}}</md-hint>
      </md-input-container>
      <div class="md-dialog-actions">
        <button md-button md-dialog-close (click)="closeDialog(false)">Cancel</button>
        <button #loginButton [disabled]="!(username && password) || loggingIn()" md-raised-button color="primary" (click)="attemptLogin(username, password)">Login</button>
      </div>
    </div>
  `
})
export class LoginDialogComponent {
  @Input() username: string = ""
  @Input() password: string = ""
  error: { username: string, password: string } = { username: "", password: "" }

  constructor(@Optional() public dialogRef: MdDialogRef<LoginDialogComponent>) { }

  /** Whether the login process is underway with server. */
  loggingIn() {
    return Accounts.loggingIn()
  }

  /** Starts the login process with the given username and password. */
  attemptLogin(username: string, password: string) {
    if (!username || !password) {
      console.error('tried to submit login form with either no username or no password')
      return
    }
    Meteor.loginWithPassword(username, password, (err: Meteor.Error) => {
      if (err) {
        console.error(err)
        this.error.username = err.reason
        this.error.password = err.reason
      } else {
        console.log('logged in successfully')
        this.closeDialog(true)
      }
    })
  }

  /** Closes the login dialog with the given success flag. */
  closeDialog(success: boolean) {
    if (this.dialogRef) {
      this.dialogRef.close(success)
    }
  }
}


import { Meteor } from 'meteor/meteor'
import { Reminders } from '../../both/collections/reminders.collection'
import { Reminder } from '../../both/models/reminder.model'

Meteor.publish('userReminders', function() {
  if (!this.userId) {
    return this.ready()
  }

  return Reminders.find({ user: this.userId })
})

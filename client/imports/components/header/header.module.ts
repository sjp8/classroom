
import { NgModule } from '@angular/core'

import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { MaterialModule } from '@angular/material'

import * as HeaderComponents from './'

const components = Object.keys(HeaderComponents).map(k => HeaderComponents[k])

@NgModule({
  imports: [MaterialModule, CommonModule, RouterModule, FormsModule],
  exports: components,
  entryComponents: [
    HeaderComponents.LoginDialogComponent
  ],
  declarations: components
})
export class HeaderModule { }
